# traj_gen

[![pipeline status](https://gitlab.inria.fr/auctus/trajectory-generation/traj_gen/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/trajectory-generation/traj_gen/-/commits/master)

Trajectory generator in Cartesian space using ruckig

<!-- [![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus%3Atrajectory-generation%3Atraj_gen)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus%3Atrajectory-generation%3Atraj_gen) -->

<!-- [![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus%3Atrajectory-generation%3Atraj_gen&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus%3Atrajectory-generation%3Atraj_gen) -->

<!-- [![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus%3Atrajectory-generation%3Atraj_gen&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus%3Atrajectory-generation%3Atraj_gen) -->

<!-- [![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus%3Atrajectory-generation%3Atraj_gen&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus%3Atrajectory-generation%3Atraj_gen) -->

<!-- [![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus%3Atrajectory-generation%3Atraj_gen&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus%3Atrajectory-generation%3Atraj_gen) -->

<!-- [![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus%3Atrajectory-generation%3Atraj_gen&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus%3Atrajectory-generation%3Atraj_gen) -->

<!-- [![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus%3Atrajectory-generation%3Atraj_gen&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus%3Atrajectory-generation%3Atraj_gen) -->

## Links

- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Atrajectory-generation%3Atraj_gen

- Documentation : https://auctus.gitlabpages.inria.fr/trajectory-generation/traj_gen/index.html

- Documentation latex : https://www.overleaf.com/7892938175rmwvvpkdyxmj

## To build 


In a catkin workspace

```terminal 
git clone https://github.com/pantor/ruckig.git
git clone git@gitlab.inria.fr:auctus/trajectory-generation/traj_gen.git
catkin build traj_gen
source devel/setup.bash
```


## To run the test

Using ros 

```terminal
roscd traj_gen
rosrun traj_gen test
```
