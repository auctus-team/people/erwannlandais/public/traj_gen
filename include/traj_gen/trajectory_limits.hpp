#pragma once

#include <Eigen/Dense>
#include "yaml-cpp/yaml.h"
#include <vector>


struct TrajectoryLimitsStruct{
     Eigen::Vector2d min_velocity;
     Eigen::Vector2d max_velocity;
     Eigen::Vector2d min_acceleration;
     Eigen::Vector2d max_acceleration;
     Eigen::Vector2d max_jerk;
};

class TrajectoryLimits {
private:
public:
     TrajectoryLimitsStruct limits;
     

     // Default constructor takes franka emika limits
     // Constant Limits should be given by a description file
     TrajectoryLimits(){
          // limits.max_jerk  << 650.0,12500;
          // limits.max_acceleration << 1.3,2.5;
          // limits.min_acceleration = - limits.max_acceleration;
          // limits.max_velocity << 1.7,2.5;
          // limits.min_velocity = - limits.max_velocity;
           setLimitFromYAMLUrl("/home/lucas/auctus_ws/dev_ros_ws/src/traj_gen/config/franka.yaml");
     }

     TrajectoryLimitsStruct getTrajectoryLimits()
     {
          return limits;
     }

     void setMaxVelocity(Eigen::Vector2d max_velocity)
     {
          limits.min_velocity = -max_velocity; 
          limits.max_velocity = max_velocity; 
     };

     void setMaxTranslationVelocity(double max_translation_velocity)
     {
          limits.min_velocity[0]= -max_translation_velocity; 
          limits.max_velocity[0] = max_translation_velocity; 
     };

     void setMaxTranslationVelocity(double min_translation_velocity,double max_translation_velocity)
     {
          limits.min_velocity[0]= min_translation_velocity; 
          limits.max_velocity[0] = max_translation_velocity; 
     };

     void setMaxRotationVelocity(double max_rotation_velocity)
     {
          limits.min_velocity[1]= -max_rotation_velocity; 
          limits.max_velocity[1] = max_rotation_velocity; 
     };

     void setMaxRotationVelocity(double min_rotation_velocity,double max_rotation_velocity)
     {
          limits.min_velocity[1]= min_rotation_velocity; 
          limits.max_velocity[1] = max_rotation_velocity; 
     };

     void setMaxVelocity(Eigen::Vector2d min_velocity,Eigen::Vector2d max_velocity)
     {
          limits.min_velocity = min_velocity; 
          limits.max_velocity = max_velocity;
     };

     void setMaxAcceleration(Eigen::Vector2d max_acceleration)
     {
          limits.min_acceleration = -max_acceleration; 
          limits.max_acceleration = max_acceleration; 
     };


     void setMaxacceleration(Eigen::Vector2d min_acceleration,Eigen::Vector2d max_acceleration)
     {
          limits.min_acceleration = min_acceleration; 
          limits.max_acceleration = max_acceleration; 
     };

     void setMaxTranslationAcceleration(double max_translation_acceleration)
     {
          limits.min_acceleration[0]= -max_translation_acceleration; 
          limits.max_acceleration[0] = max_translation_acceleration; 
     };

     void setMaxTranslationAcceleration(double min_translation_acceleration,double max_translation_acceleration)
     {
          limits.min_acceleration[0]= min_translation_acceleration; 
          limits.max_acceleration[0] = max_translation_acceleration; 
     };

     void setMaxRotationAcceleration(double max_rotation_acceleration)
     {
          limits.min_acceleration[1]= -max_rotation_acceleration; 
          limits.max_acceleration[1] = max_rotation_acceleration; 
     };

     void setMaxRotationAcceleration(double min_rotation_acceleration,double max_rotation_acceleration)
     {
          limits.min_acceleration[1]= min_rotation_acceleration; 
          limits.max_acceleration[1] = max_rotation_acceleration; 
     };

     void setMaxJerk(Eigen::Vector2d max_jerk)
     {
          limits.max_jerk = max_jerk; 
     };

     void setMaxTranslationJerk(double max_translation_jerk)
     {
          limits.max_jerk[0] = max_translation_jerk; 
     };

     void setMaxRotationJerk(double max_rotation_jerk)
     {
          limits.max_jerk[1] = max_rotation_jerk; 
     };

     void setLimitFromYAMLUrl (std::string path) 
     {
          try
          {
               YAML::Node config = YAML::LoadFile(path);
               limits.max_jerk = Eigen::Vector2d(config["CartesianLimits"]["max_jerk"].as<std::vector<double>>().data());
               limits.max_acceleration = Eigen::Vector2d(config["CartesianLimits"]["max_acceleration"].as<std::vector<double>>().data());
               limits.max_velocity = Eigen::Vector2d(config["CartesianLimits"]["max_velocity"].as<std::vector<double>>().data());
               limits.min_acceleration = -limits.max_acceleration;
               limits.min_velocity = -limits.max_velocity;
          }
          catch(std::exception& e)
          {
               std::cout << " Could not load file from url " << path << std::endl; 
          }
     }

};
